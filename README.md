# monolib-c

- toy c library for learning

- tests
    - `./tests/test.c`

- implemented
    - `src/vector`
        - auto resizing array library
    - `src/string`
        - simple string library built w/ vector
    - `src/list`
        - doubly linked list library
    - `src/map`
        - hashmap library built w/ list + vector

- not implemented
    - `src/json`
        - TODO: json serializer using libraries
    - heap
    - stack
    - queue
    - trees
    - graphs
    - string builder